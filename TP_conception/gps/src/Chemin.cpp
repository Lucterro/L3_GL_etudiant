#include "Chemin.hpp"
#include <limits>
#include <queue>
#include <sstream>
#include <fstream>

using namespace std;

/// \brief implémentation interne pour le calcul de plus court chemin
/// 
struct Parcours_ {
    Chemin cheminParcouru_;
    Chemin cheminRestant_;
    int distanceParcourue_;
};

Chemin Chemin::calculerPlusCourt(const string & ville1, 
        const string & ville2) const {

    if (routes_.empty())
        throw string("Chemin::calculerPlusCourt : routes_.empty()");

    if (ville1 == ville2)
        throw string("Chemin::calculerPlusCourt : ville1 == ville2");

    Chemin meilleurChemin;
    int meilleureDistance = std::numeric_limits<int>::max();
    queue<Parcours_> fileParcours;
    // initialise la file de parcours
    {
        Chemin cheminAvec, cheminSans;
        partitionner(ville1, cheminAvec, cheminSans);
        for (const Route & r : cheminAvec.routes_) {
            Chemin c;
            c.routes_.push_back(r);
            fileParcours.push(Parcours_{c, cheminSans, r.distance_});
        }
    }

    // teste tous les parcours
    while (not fileParcours.empty()) {
        const Parcours_ & parcoursCourant = fileParcours.front();
        const string & villeEtape 
            = parcoursCourant.cheminParcouru_.routes_.back().villeB_;
        if (villeEtape == ville2 
                and parcoursCourant.distanceParcourue_ < meilleureDistance) {
            meilleurChemin = parcoursCourant.cheminParcouru_;
            meilleureDistance = parcoursCourant.distanceParcourue_;
        }
        else if (villeEtape != ville1) {
            Chemin cheminAvec, cheminSans;
            const Chemin & cheminRestant = parcoursCourant.cheminRestant_;
            cheminRestant.partitionner(villeEtape, cheminAvec, cheminSans);
            for (const Route & r : cheminAvec.routes_) {
                Chemin c(parcoursCourant.cheminParcouru_);
                c.routes_.push_back(r);
                int d = parcoursCourant.distanceParcourue_ + r.distance_;
                fileParcours.push(Parcours_{c, cheminSans, d});
            }
        }
        fileParcours.pop();
    }

    return meilleurChemin;
}

void Chemin::partitionner(const string & ville, Chemin & cheminAvec, 
        Chemin & cheminSans) const {
	for (const Route &route : routes_){
		if (route.villeA_ == ville || route.villeB_ == ville){
			cheminAvec.routes_.push_back(Route(vile, route.villeB_, route.distance_));
		}
		else if (route.villeB_ == ville){
			cheminAvec.routes_.push_back(Route(route.villeA_, ville, route.distance_);
		}
		else {
			cheminSans.routes_.push_back(route);
	}
	 
}

void Chemin::importerCsv(istream & is) {
	std::string ligne;
	while (getline(is, ligne)){
		std::istringstream iss(ligne);
		std::string villeA, villeB, dist;
			
		Route route;
		route.villeA_ = villeB;
		route.villeB_ = villeA;
		route.distance_ = stoi(dist);
		routes_.push_back(route);
		/*getline(iss, villeA, ' ');
		getline(iss, villeB, ' ');
		getline(iss, dist, ' ');
		routes_.push_back(route(villeA, villeB, stoi(dist)));*/
	}
}

void Chemin::exporterDot(ostream & os, const string & ville1, 
        const string & ville2) const {
			Chemin plusCourt = calculerPlusCourt(ville1, ville2);
	os << "graph {" << std::endl;
	os << "spline=line" << std::endl;
	
	for(const Route &route : plusCourt.routes_){
		os << route.villeA_ << "--";
	}
	os << ville2 << "[color=red, penwidth=3];" << std::endl;
	
	for(const Route &route : routes_){
		os << route.villeA_ << "--" << route.villeB_ << "Label = " << route.distance_ << "];" << std::endl;
	}
	os << "}" << std::endl;
}

