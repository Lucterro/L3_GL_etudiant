# Drunk_player

Drunk_player est un système de lecture de vidéos qui a trop bu. Il lit les vidéos contenues dans un dossier par morceaux,
aléatoirement et parfois en transformant l'image.

 Drunk_player utilise la bibliothèque de traitement d'image <a href= "https://opencv.org/" >OpenCV</a> et est composé :

<UL> 
<li> d'une bibliothèque (drunk_player) contenant le code de base </li>
<li> d'un programme graphique (drunk_player_gui) qui affiche le résultat à l'écran </li>
<li> d'un programme console (drunk_player_cli) qui sort le résultat dans un fichier </li>
</UL>

## Dépendances

- OpenCV
- Boost

## Compilation
```shell
mkdir build
cd build
cmake ..
make
```

## Utilisation
```
./drunk_player_gui.out ../data/
```

![Video](drunk_player_gui.png)

