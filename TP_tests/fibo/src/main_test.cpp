#include <CppUTest/CommandLineTestRunner.h>
#include "Fibo.hpp"

int main(int argc, char ** argv)
{
   return CommandLineTestRunner::RunAllTests(argc, argv);
}
