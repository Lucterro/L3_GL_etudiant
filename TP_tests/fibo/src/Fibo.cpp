#include "Fibo.hpp"
#include <assert.h>

int fibo(int n, int f0, int f1) {
	assert (f0 >= 0);
	assert (f1 > 0);
	assert (f1 >= f0);
    return n<=0 ? f0 : fibo(n-1, f1, f1+f0);
}

