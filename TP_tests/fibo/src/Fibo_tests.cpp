#include "Fibo.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibonacci) { };

TEST(GroupFibonacci, test_fibo_1) { // premier test unitaire
    CHECK_EQUAL(fibo(1), 1);
}

TEST(GroupFibonacci, test_fibo_2) { // deuxième test unitaire
    CHECK_EQUAL(fibo(2), 1);
}

TEST(GroupFibonacci, test_fibo_3) { // troisième test unitaire
    CHECK_EQUAL(fibo(3), 2);
}

TEST(GroupFibonacci, test_fibo_4) { // quatrième test unitaire
    CHECK_EQUAL(fibo(4), 3);
}

TEST(GroupFibonacci, test_fibo_5) { // cinquième test unitaire
    CHECK_EQUAL(fibo(5), 5);
}
