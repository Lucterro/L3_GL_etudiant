#include "Spline.hpp"
#include <iostream>
#include <glog/logging.h>

using namespace std;

int main() {

    google::InitGoogleLogging("log_drawnspline");
    google::SetLogDestination(google::GLOG_INFO, "log_drawspline");
    LOG(INFO) << "Begin";
    LOG(INFO) << "create spline";
    Spline spline;
	
	LOG(INFO) << "add keys";
    double t;
    Vec2 P;
    while (cin >> t and cin >> P.x_ and cin >> P.y_) {
        spline.addKey(t, P);
    }
    
	LOG(INFO) << "compute values";
    double deltaT = (spline.getEndTime() - spline.getStartTime()) / 100.0;
    for (double t=spline.getStartTime(); t<spline.getEndTime(); t+=deltaT) {
        Vec2 Pt = spline.getValue(t);
        cout << t << " " << Pt.x_ << " " << Pt.y_ << "\n";
    }
	LOG(INFO) << "end";

    return 0;
}

