#include "Tictactoe.hpp"

Jeu::Jeu() {
    raz();
    afficher_plateau();
    /*while(getVainqueur() == JOUEUR_VIDE){
       jouer();
    }*/
}

void Jeu::raz() {
    tour = 0;

    for (int i(0); i<3; i++){
		for (int j(0); j<3; j++){
		plateau[i][j] = ".";
		}
	}
}

void Jeu::afficher_plateau(){
	for (int i(0); i<3; i++){
			for (int j(0); j<3; j++){
				std::cout << plateau[i][j];
			}
		std::cout << std::endl;
		}
}

std::ostream & operator<<(std::ostream & os, const Jeu & jeu) {
	/*std::string buffer;
	if (std::getline(is, buffer, ';')) 
        b._date = buffer;
	if (std::getline(is, buffer, '\n')) 
        b._volume = std::stof(buffer);
	
    os << Inv._bouteilles[i];*/
    return os;
}
Joueur Jeu::getVainqueur() const {
    // Diagnonale 1
        if( (plateau[0][0] == plateau[1][1] && plateau[0][0] == plateau[2][2] && plateau[0][0] != "." ) ){
            if(plateau[0][0] == "R"){
                return JOUEUR_ROUGE;
            }else{
                return JOUEUR_VERT;
            }
        }
        // Diagonale 2
        else if( (plateau[0][2] == plateau[1][1] && plateau[0][2] == plateau[2][0] && plateau[0][2] != "." ) ){
            if(plateau[0][2] == "R"){
                return JOUEUR_ROUGE;
            }else{
                return JOUEUR_VERT;
            }
        }else{
            for(int i = 0; i < 3; i++){
                // Ligne
                if( (plateau[i][0] == plateau[i][1] && plateau[i][0] == plateau[i][2] && plateau[i][0] != "." ) ){
                    if(plateau[i][0] == "R"){
                        return JOUEUR_ROUGE;
                    }else{
                        return JOUEUR_VERT;
                    }
                }
                // Colonne
                else if( (plateau[0][i] == plateau[1][i] && plateau[0][i] == plateau[2][i] && plateau[0][i] != "." ) ){
                    if(plateau[0][i] == "R"){
                        return JOUEUR_ROUGE;
                    }else{
                        return JOUEUR_VERT;
                    }
                }
                // Egalite
                else if( (plateau[0][i] != "." && plateau[1][i] != "." && plateau[2][i] != ".") ){
                    return JOUEUR_EGALITE;
                }
                // Pas de vainqueur, ni d'egalite
                else{
                    return JOUEUR_VIDE;
                }
            }
        }
    }


Joueur Jeu::getJoueurCourant() const {
    if(tour%2 == 0){
        return JOUEUR_ROUGE;
    }else{
        return JOUEUR_VERT;
    }
}

bool Jeu::jouer(int i, int j) {
    if(getVainqueur() == JOUEUR_VIDE){
            if( (getJoueurCourant() == JOUEUR_ROUGE) && (plateau[i][j] == ".") ){
                plateau[i][j] = "R";
                tour++;
                return true;
            }
            else if( (getJoueurCourant() == JOUEUR_VERT) && (plateau[i][j] == ".") ){
                plateau[i][j] = "V";
                tour++;
                return true;
            }
        }
        return false;
}

